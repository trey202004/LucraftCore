package lucraft.mods.lucraftcore.superpowers.effects;

import com.google.gson.JsonObject;
import lucraft.mods.lucraftcore.superpowers.abilities.Ability;
import lucraft.mods.lucraftcore.superpowers.abilities.Ability.AbilityType;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.util.JsonUtils;
import net.minecraft.util.ResourceLocation;

public class EffectConditionAbilityEnabled extends EffectCondition {

    public ResourceLocation ability;

    @Override
    public boolean isFulFilled(EntityLivingBase entity) {
        for (Ability ab : Ability.getAbilities(entity)) {
            if (ab.getAbilityEntry().getRegistryName().equals(ability) && ab.isUnlocked()) {
                if (ab.getAbilityType() == AbilityType.CONSTANT)
                    return true;
                if (ab.isEnabled())
                    return true;
            }
        }
        return false;
    }

    @Override
    public void readSettings(JsonObject json) {
        this.ability = new ResourceLocation(JsonUtils.getString(json, "ability"));
    }

}
